function renderDSSV(dssv) {
  var contentHTML = "";
  for (var i = 0; i < dssv.length; i++) {
    var contentTr = `<tr>
      <td>${dssv[i].ma}</td>
      <td>${dssv[i].ten}</td>
      <td>${dssv[i].email}</td>
      <td>${dssv[i].tinhDTB()}</td>
      <td>
        <button onclick="xoaSV(${dssv[i].ma})" class="btn btn-danger">Xóa</button>
        <button onclick="suaSV(${dssv[i].ma})" class="btn btn-warning">Sửa</button>
      </td>
    </tr>`;
    contentHTML += contentTr;
  }

  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

function layThongTinTuForm() {
  var ma = document.getElementById("txtMaSV").value;
  var ten = document.getElementById("txtTenSV").value;
  var email = document.getElementById("txtEmail").value;
  var matkhau = document.getElementById("txtPass").value;
  var toan = document.getElementById("txtDiemToan").value;
  var ly = document.getElementById("txtDiemLy").value;
  var hoa = document.getElementById("txtDiemHoa").value;

  // return {
  //   ma: ma,
  //   ten: ten,
  //   email: email,
  //   matkhau:matkhau,
  // toan: toan,
  //   ly: ly,
  //   hoa: hoa,
  // };

  return new SinhVien(ma, ten, email, matkhau, toan, ly, hoa);
}
