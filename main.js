var dssv = [];
const DSSV_LOCAL = "DSSV_LOCAL";
// khi user load trang ==> lấy dữ liệu từ localstorage
var jasonData = localStorage.getItem(DSSV_LOCAL);
if (jasonData != null) {
  // JSON.parse(jsonData)=> Array
  // convert array cũ (lấy localStorage) => không có key tinhDTB() => khi lưu xuống bị mất => khi lấy lên không còn => convert thành array mới
  dssv = JSON.parse(jasonData).map(function (item) {
    // item : phần tử của array trong các lần lặp
    // map bắt buộc phải có return
    return new SinhVien(
      item.ma,
      item.ten,
      item.email,
      item.matKhau,
      item.toan,
      item.ly,
      item.hoa
    );
  });
  console.log(`  🚀: dssv`, dssv);
  renderDSSV(dssv);
  // map js
}

function themSV() {
  var sv = layThongTinTuForm();

  dssv.push(sv);
  // convert data
  var dataJson = JSON.stringify(dssv);
  // lưu xuống localStorage
  localStorage.setItem(DSSV_LOCAL, dataJson);
  // render dssv lên table
  renderDSSV(dssv);
  resetForm();
}

function xoaSV(id) {
  for (var i = 0; i < dssv.length; i++) {
    var viTri = -1;
    if (dssv[i].ma == id) {
      viTri = i;
    }
  }
  dssv.splice(viTri, 1);

  var dataJson = JSON.stringify(dssv);
  // lưu xuống localStorage
  localStorage.setItem(DSSV_LOCAL, dataJson);
  renderDSSV(dssv);
}

function suaSV(id) {
  var viTri = dssv.findIndex(function (item) {
    return item.ma == id;
  });

  document.getElementById("txtMaSV").value = dssv[viTri].ma;
  document.getElementById("txtTenSV").value = dssv[viTri].ten;
  document.getElementById("txtEmail").value = dssv[viTri].email;
  document.getElementById("txtDiemToan").value = dssv[viTri].toan;
  document.getElementById("txtDiemLy").value = dssv[viTri].ly;
  document.getElementById("txtDiemHoa").value = dssv[viTri].hoa;
}

function capNhatSV() {
  var sv = layThongTinTuForm();
  var viTri = dssv.findIndex(function (item) {
    return item.ma == sv.ma;
  });

  dssv[viTri] = sv;
  var dataJson = JSON.stringify(dssv);
  // lưu xuống localStorage
  localStorage.setItem(DSSV_LOCAL, dataJson);
  renderDSSV(dssv);
}

function resetForm() {
  document.getElementById("formQLSV").reset();
}
